# dotfiles

## Installation

Inspired by [How to Store Dotfiles - A Bare Git Repository][1].

[1]: https://www.atlassian.com/git/tutorials/dotfiles

### Install into new system

```
git clone --bare https://gitlab.com/pedrodavid96/dotfiles.git $HOME/Documents/dotfiles
alias dot='git --git-dir=$HOME/Documents/dotfiles --work-tree=$HOME'
dot checkout
```

Might fail with the following:
```
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .profile
Please move or remove them before you can switch branches.
Aborting
```

In that case the following script might help to create backups:
```
mv .bashrc{,.backup} && mv .profile{,.backup}
```

### Starting from scratch

```
git init --bare $HOME/.cfg
alias dot='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
```

## Decision Records

### Favor `.gitignore` with `/*` to `showUntrackedFiles no`

This avoids adding files by mistake.
Additinoally, it will show changes in the `~/.config` folder as it is
not ignored.

### `export` variables in `.profile`

.profile is the first "entry point" (login) and will set a few
environment variables that should be inherited by all child processes.

### No `.zprofile` or `.zlogin`

~~I won't have ZSH setup as a "login shell" (i.e.: I won't ever login
directly into zsh) so this doesn't seem necessary and avoids
complexity.~~

Actually in OSX it seems ZSH always acts as a login shell.
Trying to make some ZSH compilation optimizations to speed things up.

### Downside of git bare repo for dotfiles

Alias doesn't seem to work in `xargs`.
It's very common for me to run `git gone | xargs git branch -d` to
delete branches that are no longer in the remote.
This doesn't seem to work with `xargs dot branch -d`.

Still easy to run from the dotfiles git folder but minor nuisance.
There might be a better approach for it even.

### Remove executable flag from shell startup scripts

While trying to understand how to lint my dotfiles I found
[this comment](https://github.com/koalaman/shellcheck/issues/581#issuecomment-190539448)
which states:

`Nobody would be silly enough to use 'chmod +x' on a module... ahem`

Well... I was... and being completely honest anything wrong came from
it and I couldn't find anything stating that I shouldn't besides this
comment... but I don't want to be silly... so...

Honestly I simply checked an example from the system (`/etc/profile`),
which didn't have the flag, so I'm removing accordingly.

Additionally, from that same thread I've understood that, conceptually,
since these scripts are only meant to be sourced and not actually
executed, the executable flag (`chmod +x`) and the shebang doesn't make
sense.

_Side Note:_
A curiosity while making this change...
> symlink permissions serve no purpose on most OSes, including Linux.
> The execution permissions that are taken into account are the ones of
> the file linked to, if any.
From https://unix.stackexchange.com/a/494705

### Follow [flowblok | shell startup scripts](https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html) examples

While complex (by the nature of the underlying technology), the explanation examples in this guide try to unify shells
into a "standard" startup order.

Hopefully by following the examples we can arrive at a simpler shell startup scripts structure.

## Troubleshooting

### Mac sign git commits

https://stackoverflow.com/a/40066889

Adapted, only required:
```
brew install pinentry-mac
echo "pinentry-program $(brew --prefix)/bin/pinentry-mac" > ~/.gnupg/gpg-agent.conf
```

Didn't require any git config change.

Actually needed to change configurations to disable the check mark of saving to _Mac Keychain_ being enabled by default:
```
defaults write org.gpgtools.common UseKeychain NO
```

### Bare repo not tracking remote origin correctly

Couldn't fetch branch from origin.
Solved with:

```bash
dot config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
```
