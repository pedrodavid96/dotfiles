# vim

Install [vim-plug](https://github.com/junegunn/vim-plug).

At the time of writing (adapted to XDG):
```
curl -fLo ${XDG_CONFIG_HOME}/vim/autoload/plug.vim --create-dirs \
   https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Open vim and:
```
:PlugInstall
```
