call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sleuth'
Plug 'airblade/vim-gitgutter'


call plug#end()

syntax on

" show relative line numbers with current line
set number
set relativenumber

" show command in status bar
set showcmd

" always show status bar
set laststatus=2

" show menu with file wildcard matches
set wildmenu

" highlight search
set hlsearch

" don't go to begin of file after search hits bottom
set nowrapscan

function MyFTypeGitCommit()
  " Check spelling.
  setlocal spell
  
  " Margin for emails, make it obvious where 72 characters is.
  setlocal textwidth=72
  setlocal colorcolumn=+1
endfun

autocmd FileType gitcommit call MyFTypeGitCommit()
