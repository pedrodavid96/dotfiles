. "${XDG_CONFIG_HOME:-$HOME/.config}/bash/env"

# 2. Prevent it from being run later, since we need to use $BASH_ENV for
# non-login non-interactive shells.
# We don't export it, as we may have a non-login non-interactive shell as
# a child.
# **Personal Note:**
# Seems to just avoid runing bash env again in a child of a bash login shell, doesn't seem critical.
BASH_ENV=

. "${XDG_CONFIG_HOME:-$HOME/.config}/bash/login"

if [[ $- == *i* ]]; then
    . "${XDG_CONFIG_HOME:-$HOME/.config}/bash/interactive"
fi
